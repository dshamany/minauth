podman_cleanup:
	podman pod stop -a && podman pod rm -a && reset

podman_backend_with_mongo:
	podman-compose -f docker-compose.yml -f service-develop.yml -f mongo-develop.yml up --build

podman_backend_with_mongo_testing:
	podman-compose -f docker-compose.yml -f service-develop.yml -f mongo-develop.yml -f mongo-testing.yml -f test-continuous.yml up --build

podman_fullstack:
	podman-compose -f docker-compose.yml -f client-develop.yml -f service-develop.yml -f mongo-develop.yml up --build

podman_client:
	podman-compose -f docker-compose.yml -f client-develop.yml up --build

docker_cleanup:
	reset

docker_backend_with_mongo:
	docker-compose -f docker-compose.yml -f service-develop.yml -f mongo-develop.yml up --build

docker_fullstack:
	docker-compose -f docker-compose.yml -f client-develop.yml -f service-develop.yml -f mongo-develop.yml up --build

docker_client:
	docker-compose -f docker-compose.yml -f client-develop.yml up --build