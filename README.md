# Quick Start

## Build and run project in containers

`docker-compose up`

This will also run the unittests in parallel to the application. To run just the tests once, switch to a different env file:

`docker-compose --env-file test-once.env up`

## Check if user already exists

```bash
curl http://<url>:3000/api/users/emailCheck/<email_address>
```

## Create new user with cUrl

```bash
curl -X POST  \
-H "Content-Type: application/json" \
-d '{"email": "<email-as-string>", "username": "<username-as-string>"}' \
http://<url>:3000/api/users/
```

## Send code to user

This will email a code to the user independently from the third-party attempting to access the user information.

```bash
curl http://<url>:3000/api/users/sendCode/<username>
```

## Send API key to user

This will email a code to the user independently from the third-party attempting to access the user information.

```bash
curl http://<url>:3000/api/users/sendApiKey/<username>
```

## Verify user with cUrl

The payload returned here is in the format `{"_id": <user-id-as-string>, "verified": true}`.

```bash

curl -X POST  \
-H "Content-Type: application/json" \
-d '{"username": "<username-as-string>", "tmp_code": "<code-as-string>"}' \
http://<url>:3000/api/users/verify
```

## Retrieve user information

This will return a payload with first-, and lastname, along with email and phone number.

```bash
curl http://<url>:3000/api/users/<user-id>
```

**Client Port:** 4000
**Server Port:** 3000
**MongoDB Port:** 27017 (default)