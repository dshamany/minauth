import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import CredentialsView from './components/Credentials/CredentialsView';
import UserDetailsView from './components/UserDetails/UserDetailsView';

function App() {
  return (
    <div className="App">
      <div className="mainContainer" style={mainContainer}>
      <Router>
        <Route path="/signin" render={() => <CredentialsView />}/>
        <Route path="/user" render={() => <UserDetailsView />} />
      </Router>
      </div>
    </div>
  );
}

const mainContainer = {
  height: "100vh",
  width: "100vw",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  background: "url('https://www.zastavki.com/pictures/originals/2015/Nature___Forest_Blue_fog_in_the_forest_103104_.jpg')",
  backgroundPosition: "center",
  backgroundRepeat: "no-repeat",
  backgroundAttachment: "fixed"
}

export default App;
