export async function sendCode(emailAddress){
    const payload = {
        email: emailAddress
    }
    await fetch('http://localhost:3000/api/users/', {
        method: "POST",
        headers: {
            'Content-Type':'application/json'
        },
        body: JSON.stringify(payload)
    });
}

export async function signIn(emailAddress, code) {
    const payload = {
        email: emailAddress,
        tmp_code: code
    }
    const rawResponse = await fetch('http://localhost:3000/api/users/verify', {
        method: "POST",
        headers: {
            'Content-Type':'application/json',
            'Authorization':'e223a69c-3d27-446c-ac14-910bed110bc4'
        },
        body: JSON.stringify(payload)
    });

    const jsonResponse = await rawResponse.json();
    console.log(jsonResponse);

    const rawUserInfo = await fetch(`http://localhost:3000/api/users/${jsonResponse.one_time_code}`, {
        method: "GET",
        headers: {
            'Accept':'application/json',
            'Authorization':'e223a69c-3d27-446c-ac14-910bed110bc4'
        }
    });

    const jsonUserInfo = await rawUserInfo.json();
    localStorage.setItem('user', JSON.stringify(jsonUserInfo));
}