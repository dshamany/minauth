import React, { useState } from 'react';
import { getUserFromLocalStorage } from '../UserDetails/UserDetailsController';
import { sendCode, signIn } from './CredentialsViewController';

const CredentialsView = ({ props }) => {

    const [email, setEmail] = useState("");
    const [code, setCode] = useState("");
    const [signInMode, setSignInMode] = useState(false)

    return (
        <div className="credentialsContainer" style={styles.container}>
            <h1 style={styles.h1}>__Min_Auth__</h1>
            <div className="innerContainer" style={styles.innerContainer}>
                <input style={styles.textInput}
                    placeholder="Email Address"
                    onChange={(e) => {
                        setEmail(e.target.value);
                    }}
                    value={email}
                    hidden={signInMode}
                />
                <input style={styles.textInput}
                    placeholder="Code"
                    onChange={(e) => {
                        setCode(e.target.value);
                    }}
                    value={code}
                    hidden={!signInMode}
                />
            </div>
            <button 
            style={styles.button}
            hidden={signInMode}
            onClick={() => {
                setSignInMode(!signInMode);
                sendCode(email);
            }}
            >Send Code</button>
            <button 
            style={styles.button}
            hidden={!signInMode}
            onClick={() => {
                signIn(email, code);
                setEmail("");
                setCode("");
                setSignInMode(!signInMode);
                if (getUserFromLocalStorage())
                    window.location.pathname = "/user";
            }}
            >Sign In
            </button>
        </div>
    )
}

const styles = {
    container: {
        width: 360,
        height: 320,
        backgroundColor: "#333333ee",
        padding: 10,
        border: "3px solid #fff"
    },
    innerContainer: {
        height: "70%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-around",
        alignItems: "center"
    },
    h1: {
        height: 30,
        margin: 5,
        color: "white",
        fontWeight: "normal",
    },
    textInput: {
        height: 30,
        width: 250,
        border: "1px solid white",
        backgroundColor: "transparent",
        color: "white",
        padding: 5,
        margin: 5,
        textAlign: "center",
        fontSize: 16
    },
    button: {
        border: "1px solid #fff",
        padding: 5,
        width: 100,
        height: 40,
    }
}

export default CredentialsView;