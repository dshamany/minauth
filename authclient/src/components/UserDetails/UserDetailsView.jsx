import React, { useState } from 'react';
import { getUserFromLocalStorage, logout, updateUserLocally } from './UserDetailsController';


const UserDetailView = ({props}) => {
    const user = getUserFromLocalStorage();

    const [firstname, setFirstname] = useState(user.first_name);
    const [lastname, setLastname] = useState(user.last_name);
    const [email, setEmail] = useState(user.email);
    const [phone, setPhone] = useState(user.phone);

    return (
    <div className="userContainer" style={styles.userContainer}>
        <div className="userInfo" style={styles.userInfo}>
            <input 
                style={styles.textInput}
                type="text" 
                placeholder="First Name"
                value={firstname}
                onChange={(e) => setFirstname(e.target.value)} />
            <input 
                style={styles.textInput}
                type="text" 
                placeholder="Last Name" 
                value={lastname}
                onChange={(e) => setLastname(e.target.value)} />
            <input
            style={styles.textInput} 
                type="email" 
                placeholder="Email Address" 
                value={email}
                onChange={(e) => setEmail(e.target.value)} />
            <input 
                style={styles.textInput}
                type="tel" 
                placeholder="Phone Number"
                value={phone}
                onChange={(e) => setPhone(e.target.value) } />
        </div>
        <div className="userActions">
        <button
            style={styles.button}
            onClick={() => {
                const updated = {
                    first_name: firstname,
                    last_name: lastname,
                    phone: phone,
                    email: email
                }
                updateUserLocally(updated);
            }}
            >Save Changes
            </button>
            <button
            style={styles.button}
            onClick={() => {
                logout();
            }}
            >Sign Out
            </button>
        </div>
    </div>
)}


const styles = {
    userContainer: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-evenly",
        alignItems: "center",
        height: 340,
        width: 380,
        backgroundColor: "#000000dd"
    },
    userInfo: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        alignItems: "center",
        height: 150,
        width: 300
    },
    textInput: {
        height: 30,
        width: 250,
        border: "1px solid white",
        backgroundColor: "transparent",
        color: "white",
        padding: 5,
        margin: 5,
        textAlign: "center",
        fontSize: 16
    },
    button: {
        border: "1px solid #fff",
        padding: 5,
        width: 100,
        height: 40,
    }
}

export default UserDetailView;