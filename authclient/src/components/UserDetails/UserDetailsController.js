export function getUserFromLocalStorage(){
    return JSON.parse(localStorage.getItem('user'));
}

export function setUserInfo(key, value){
    let user = getUserFromLocalStorage();
    user[key] = value;
    localStorage.setItem('user', user);
}

export function updateUserLocally(user){
    localStorage.setItem('user', JSON.stringify(user));
}

export function logout(){
    localStorage.removeItem('user');
    window.location.pathname = "/signin"
}