let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const { uuidv4, oneTimeCode } = require('../controllers/utilities');

const userSchema = new Schema({
	first_name: {
		type: String,
		default: ""
	},
	last_name: {
		type: String,
		default: ""
	},
	phone: {
		type: String,
		default: ""
	},
	email: {
		type: String,
		required: true,
		unique: true
	},
	tmp_code: {
		type: String,
		default: ""
	},
	created_on: Date,
	code_exp: Date,
	last_login: Date,
	api_key: {
		type: String,
		unique: true,
		default: uuidv4()
	},
	client_code: {
		type: String,
		unique: true,
		default: oneTimeCode()
	}
});

module.exports = mongoose.models.User || mongoose.model('User', userSchema);
