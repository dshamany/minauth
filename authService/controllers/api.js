const serverIsOnline = (req, res) => {
	res.send({ success: true })
};

module.exports = {
	serverIsOnline,
}