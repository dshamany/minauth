const CONSTANTS = {
    CODE_EXPIRATION_IN_MINUTES: 10,
    HEADERS: {
        AUTHORIZATION : "authorization",
        CONTENT_TYPE: "content-type",
    }
}

module.exports = {
    CONSTANTS
}