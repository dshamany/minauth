const MESSAGES = {
    MISSING: {
        // the HEADER string is used in conjunction with header name
        HEADER: "header is missing.",
        API_KEY: "API Key is missing.",
        JSON_HEADER: "Don't forget to add \'Accept: application/json\' to your POST request.",
        AUTHORIZATION_HEADER: "Authorization header is missing (e.g. \'Authorization: <api_key>\')",
        USER: "User does not exist.",
        EMAIL: "Email is missing",
        CODE: "User code is missing"
    },
    INVALID: {
        API_KEY: "Invalid API Key",
        AUTHORIZATION_HEADER: "Invalid authorization header (e.g. \'Authorization: <api_key>\')",
        USER: "Invalid user.",
        TMP_CODE: "Invalid user code",
        CLIENT_CODE: "Invalid Client Code",
    },
    HEADERS: {
        ACCEPT: "Accept",
        AUTHORIZATION: "Authorization",
        CONTENT_TYPE: "Content-Type"
    }
}

const LOG = {
    CLIENT: {
        AUTHORIZED: "Client authorized.",
        UNAUTHORIZED: "Client unauthorized.",
    }
}

module.exports = {
    MESSAGES,
    LOG
}