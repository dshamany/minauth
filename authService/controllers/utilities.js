/**
 * This file is to group commion utilities
 */

function random6Digits() {
	return (Math.random() * 999999).toFixed().padStart(6, '0');
}

// uuid package did not work, so this is a temporary work-around
function uuidv4() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
}

// temporary user id for one-time use by client
function oneTimeCode(codeLength = 32){
	const acceptableCharacters = [
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
		'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
		'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
		'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
		'W', 'X', 'Y', 'Z'
	]

	let result = "";
	let current = 0;
	for (let i = 0; i < codeLength; i++){
		current = (Math.random() * acceptableCharacters.length).toFixed();
		result += acceptableCharacters[current];
	}
	return result;
}

// Date Handlers

function dateInMs(date){
	return date.getTime();
} 

function currentDateInMS() {
	return Date.now();
}

function appendCurrentDate(milliseconds) {
	return Date.now() + milliseconds;
}

// Conversions
function hoursToMinutes(hours) {
	return hours * 60;
}

function minutesToSeconds(minutes){
	return minutes * 60;
}

function secondsToMs(seconds) {
	return seconds * 1000;
}

function hoursToMs(hours) {
	return secondsToMs(minutesToSeconds(hoursToMinutes(hours)));
}


function minutesToMs(minutes) {
	return secondsToMs(minutesToSeconds(minutes));
}

// validation
function isExpired(date){
	// user dateInMs() to ensure that the date received
	// from mongoDB is in milliseconds
	return currentDateInMS() > dateInMs(date);
}

module.exports = {
	random6Digits,
	uuidv4,
	currentDateInMS,
	appendCurrentDate,
	hoursToMinutes,
	minutesToSeconds,
	secondsToMs,
	hoursToMs,
	minutesToMs,
	isExpired,
	oneTimeCode
}
