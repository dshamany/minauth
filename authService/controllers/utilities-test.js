const assert = require('assert');

const _ = require('underscore');

const { random6Digits } = require('./utilities');

// Some helper functions for evaluating the randomness of random6Digits.
const add = (a, b) => a + b;
const sum = collection => _.reduce(collection, add, 0);

// Multiple dice throws follow a multinomial distribution. pEquinomial returns
// the probability of a given outcome of such an experiment, under the
// assumption that the thrown die is fair. `tally` must be an array with as many
// elements as the dice have sides, containing the number of times each side was
// thrown.
function pEquinomial(tally) {
	const k = tally.length;
	if (k < 2) return 1;
	tally = tally.slice();
	let n = sum(tally);
	let x = tally.pop();
	let p = 1;
	// The direct formula is p = n! / (x[0]! * ... * x[k-1]!) * (1/k)**n, but a
	// naieve implementation of this formula will quickly reach the limits of a
	// double precision float. The loop below ensures that we only lose
	// precision when we're sure that the probability must be very close to
	// zero.
	while (x || tally.length) {
		while (n > k && p < 1e250) p *= n-- / k;
		while (x && (p > 1e-250 || n === k)) p /= x--;
		if (!x) x = tally.pop();
	}
	while (n) p *= n-- / k;
	return p;
}

describe('controllers/utilities', function() {
	describe('pEquinomial', function() {
		it('checks against easily verified multinomials', function() {
			assert(pEquinomial([0, 1]) === .5);
			assert(pEquinomial([1, 0]) === .5);
			assert(pEquinomial([1, 1]) === .5);
			assert(pEquinomial([0, 2]) === .25);
			assert(pEquinomial([2, 0]) === .25);
			assert(pEquinomial([3, 0]) === .125);
			assert(pEquinomial([4, 0]) === .0625);
			assert(Math.abs(pEquinomial([0, 2, 1]) - 1/9) < 1e-200);
			assert(pEquinomial([0, 1, 0, 0]) === .25);
			assert(Math.abs(pEquinomial([0, 1000]) - .5**1000) < 1e-307);
		});

		it('always returns a valid probability for valid inputs', function() {
			_.times(100, () => {
				const k = _.random(100);
				let n = _.random(10000);
				const tally = _.times(k, (i) => {
					if (i === k - 1) return n;
					const x = _.random(n);
					n -= x;
					return x;
				});
				const p = pEquinomial(tally);
				assert(tally && p >= 0 && p <= 1);
			});
		});
	});

	describe('random6Digits', function() {
		it('always returns a string of six digits', function() {
			assert(_.isFunction(random6Digits));
			// We try the assertion 30 times because this reduces the
			// probability of a false positive to less than 5% (.9^30).
			_.times(30, () => assert(random6Digits().length === 6));
		});

		it('draws six-digit strings from a uniform distribution', function() {
			// Our null hypothesis is that we're throwing six independent,
			// unbiased ten-sided dice. To test, we start by throwing them 10k
			// times and tallying the number of times that each die landed on
			// each side.
			const tally = [[], [], [], [], [], []];
			_.times(10000, () => _.each(random6Digits(), (digit, position) => {
				tally[position][+digit] = (tally[position][+digit] || 0) + 1;
			}));
			// Next, we check that the outcome is not too unlikely. With a
			// properly implemented random6Digits, this will produce a false
			// negative only seldomly. With an improper implementation, it will
			// fail nearly every time. The p-values are so small because at
			// this sample size, there are many ways to get a similarly
			// semi-homogeneous distribution.
			_.each(tally, (t) => {
				// We also ask that every die rolls on each side at least once.
				// If this doesn't happen, we can confidently reject the null
				// hypothesis.
				_.each(t, x => assert(x));
				assert(pEquinomial(t) > 1e-21);
			});
			// Combine the distributions of all six digits. This gives an even
			// greater sample size, hence an even smaller p-value threshold.
			assert(pEquinomial(_.map(_.unzip(tally), sum)) > 1e-23);
		});
	});
});
