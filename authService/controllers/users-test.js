// const assert = require('assert');

// const _ = require('underscore');
// var mongoose = require('mongoose');

// require('../configs/mongo-test');
// const User = require('../models/user');
// const { createUser } = require('./users');
// const { testApiKey, createTestUser } = require('../test-util');

// describe('users', function() {
// 	beforeEach(createTestUser);
// 	afterEach(async function() {
// 		await User.remove({ email: /.+/ });
// 	});

// 	describe('createUser', function() {
// 		it('confirms the email address of the created user', async function() {
// 			let result;
// 			const mockRequest = {
// 				headers: { authorization: testApiKey },
// 				body: { email: 'test@test.com', username: 'test_acct' },
// 			};
// 			const mockResponse = {
// 				send(obj) {
// 					result = obj;
// 				},
// 			};
// 			await createUser(mockRequest, mockResponse);
// 			assert(result.created_for === 'test@test.com');
// 		});

// 		it('cannot be tricked into storing additional fields', async function(){
// 			const mockRequest = {
// 				headers: { authorization: testApiKey },
// 				body: {
// 					email: 'test@test.com',
// 					username: 'test_acct',
// 					api_key: 'abcd',
// 				},
// 			};
// 			const mockResponse = { send: _.noop };
// 			await createUser(mockRequest, mockResponse);
// 			const user = await a$.waterfall([
// 				a$.constant({ username: 'test_acct' }),
// 				User.findOne.bind(User),
// 			]);
// 			assert(user.api_key !== 'abcd');
// 		});
// 	});
// });
