const User = require('../models/user');
const { sendCode, sendAPIKey } = require('../configs/nodemailer');
const {
	uuidv4,
	random6Digits,
	appendCurrentDate,
	minutesToMs,
	isExpired,
	oneTimeCode,
	currentDateInMS,
} = require('../controllers/utilities');

const _ = require('underscore');
const SHA512 = require('js-sha512');
const { LOG, MESSAGES } = require('./messages');
const { CONSTANTS } = require('./constants');

// HELPERS
const validHeader = (header, headerName) => {
	return new Promise((resolve, reject) => {
		if (!!header){
			return resolve(header);
		}
		reject(headerName+" "+MESSAGES.MISSING.HEADER);
	})
}

// Helper function to verify api key
const authorizeClient = (apiKey) => {
	// convert key to hash512
	const hashedKey = SHA512(apiKey);
	// find user with the correct hashedKey
	return User.findOne({api_key: hashedKey});
};

// FUNCTIONS START BELOW
const apiDesc = (req, res) => {
	res.send({
		get: {
			prefix: "/",
			action: "api information"
		},
		post: {
			prefix: "/",
			action: "create user"
		},
		put: {
			prefix: "/:id",
			action: "update user information"
		},
		delete: {
			prefix: "/:id",
			action: "delete user",
		}
	});
}

const updateUserAndSendCode = (user, res) => {
	// generate new 6 digit code
	const code = random6Digits();
	const hashedCode = SHA512(code);

	// create expiration date for code
	const expiration = new Date(appendCurrentDate(minutesToMs(CONSTANTS.CODE_EXPIRATION_IN_MINUTES)));

	// create one-time access for client
	let accessCode = oneTimeCode();

	// create updated user object
	const update = {
		tmp_code: hashedCode, 
		code_exp: expiration,
		client_code: accessCode,
		last_login: Date(currentDateInMS)
	};

	User.findByIdAndUpdate(user._id, update, (err, updatedUser) => {
		if (err){ return res.send({error: MESSAGES.MISSING.USER}); }

		// send code to user
		// returns boolean
		sendCode(updatedUser.email, code)
		.then(code_sent => res.send({code_sent}));
	});
};

const createUserAndSendCode = (email, res) => {
	// create new 6 code
	const code = random6Digits();
	const hashedCode = SHA512(code);

	// create expiration for code
	const expiration = Date(appendCurrentDate(minutesToMs(CONSTANTS.CODE_EXPIRATION_IN_MINUTES)));

	// create one time access code for client
	const accessCode = oneTimeCode();

	// create user object
	const user = new User({
		email: email,
		tmp_code: hashedCode,
		code_exp: expiration,
		client_code: accessCode,
		created_on: Date(currentDateInMS),
		last_login: Date(currentDateInMS)
	});

	try {
		// save user to mongodb
		user.save();
		// send 6 digit code (unhashed) to user email
		// returns boolean
		return sendCode(user.email, code)
		.then(code_sent => res.send({code_sent}));
		
	} catch (err) {
		// handle error
		return ({error: "Could not save user"});
	}
	
};

const processEmail = (req, res) => {
	if (!req.body.email){ return res.send({error: MESSAGES.MISSING.EMAIL}); }
	// update or create user depending on the existance of their
	// email in mongodb
	User.findOne({email: req.body.email}, (err, userFound) => {
		if (err) { return res.send({error: MESSAGES.INVALID.USER}); }
		if (userFound){
			updateUserAndSendCode(userFound, res);
		 } else {
			 createUserAndSendCode(req.body.email, res);
		 }
	});
}

const getUser = (req, res) => {
	validHeader(req.headers.authorization, CONSTANTS.HEADERS.AUTHORIZATION)
	.then(authorization => authorizeClient(authorization))
	.then(validClient => {
		if (validClient){
			let accessCode = req.params.accesscode;
			User.findOne({client_code: accessCode}, (err, user) => {
				if (err){ throw err; }

				// handle error where client code is wrong
				// which would fail to return user
				if (!user){
					return res.send(MESSAGES.INVALID.CLIENT_CODE);
				}

				// erase client code
				user.client_code = "";
				user.save();

				// send a subset of the user object to protect user integrity
				res.send(_.pick(user, 'first_name', 'last_name', 'email', 'phone'));
			});
		} else {
			res.send(MESSAGES.MISSING.API_KEY);
		}
	})
	.catch (error => {
		res.send(error);
	})
}

const updateUser = (req, res) => {
	// console.log(req.body);
}

const deleteUser = (req, res) => {
}

const getApiKey = (req, res) => {
	let key = uuidv4();
	// update user api_key when this function is called
	// it is the user's job to keep their api_key safe
	User.findOne({ email: req.params.email }, (err, user) => {
		if (err) { res.send({ err }); return; }
		user.api_key = SHA512(key);

		// update user
		User.findByIdAndUpdate(user._id, user, (err, updatedUser) => {
			if (err) { res.send({ err }); return; }

			// nodemailer should send the key to user email here
			sendAPIKey(user.email, key);

			// this will temporarily send the key as a response
			// due to lack of functioning nodemailer implementation
			res.send({ api_key_sent_to: user.email });
		});
	});

};

const verifyUser = (req, res) => {
	// check for valid header
	validHeader(req.headers.authorization, "Authorization")
	// ensure authorization of client
	.then(authorization => authorizeClient(authorization))
	// take action: return a verified user
	.then((validClient) => {
		if (validClient){
			// hash code used to identify user
			const hashedCode = SHA512(req.body.tmp_code);
			// find user with email and hashedCode
			User.findOne({$and: [{email: req.body.email}, {tmp_code: hashedCode}]}, (err, user) => {
					// throw error to catch block
					if (err){ throw err; }

					// handle error where user doesn't exists
					if (!user){
						return res.send(MESSAGES.MISSING.USER);
					}
					// ensure tmp_code is not expired
					if(!isExpired(user.code_exp)){
						// user's temporary access (client_code) was updated
						// when the tmp_code was sent
						// return the code to the client
						res.send({one_time_code: user.client_code})
					} else {
						// inform client if code is too old
						res.send(MESSAGES.INVALID.TMP_CODE)
					}
				});
		} else {
			res.send(MESSAGES.INVALID.API_KEY);
		}
	})
	.catch(err => {
		res.send(err);
	});
}

module.exports = {
	apiDesc,
	processEmail,
	getUser,
	updateUser,
	deleteUser,
	getApiKey,
	verifyUser,
}