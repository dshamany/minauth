let express = require('express');
let router = express.Router();

const { serverIsOnline } = require('../controllers/api');

// this route is for testing
router.get('/', serverIsOnline);

module.exports = router;
