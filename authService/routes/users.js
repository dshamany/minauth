var express = require('express');
var router = express.Router();

const {  
	getUser,
	updateUser,
	deleteUser,
	verifyUser,
	apiDesc,
	getApiKey,
	processEmail,
} = require('../controllers/users');

/** 
 * This is not REST compliant for testing reasons
*/

/**
 * FLOW:
 * - Enter Email -> check if email exists
 * 		- create new user with only email parameter if none exist
 * 		- alternatively, request a 6 digit code to be sent to user
 * - Present user with code field and submit both 'email' and 'tmp_code'
 * 	 parameters in the POST body to verify user, which returns the user
 *   object in the response
 */

// API Description for "/users" route
router.get('/', apiDesc);

// READ USER
// this route returns user info without user api key if ID is known
// the user has other means of accessing their own account via 
// sign in flow
router.get('/:accesscode', getUser);

// this route sends the api key to the user
router.get('/sendApiKey/:email', getApiKey);

// PROCESS EMAIL FOR USER (CREATE OR UPDATE)
router.post('/', processEmail);

// UPDATE USER
// this route is ensures user information is updated
// by a verified api key holder
// although, at the moment, an API key is not required
router.put('/:id', updateUser);

// DELETE USER
// this route should only be possible from user UI
// and since it is dangerous, should require user
// to generate an API Key to validate a delete
// (implementation is incomplete here)
router.delete('/:id', deleteUser);

// this is where the user signs in
// the user is verified by tmp_code and email
// the client must therefore send a post with both
// 'email' and 'tmp_code' parameters in the request
// body
router.post('/verify', verifyUser);

module.exports = router;