const nodemailer = require('nodemailer');

let testAccount;

async function createTestAccount(){
	// this is only used in place of a real account
	// and will not be required in production
	testAccount = await nodemailer.createTestAccount();
}

async function sendMail(to, subject, html_msg) {

	let acct = await nodemailer.createTestAccount();

	// set transporter to reflect test account
	const transporter = nodemailer.createTransport({
		host: 'smtp.ethereal.email',
		port: 587,
		auth: {
			user: process.env.USER,
			pass: process.env.PASS
		}
	});
	// create an object to send via mail with all
	// parameters disclosed in the object
	let info = await transporter.sendMail({
		from: "ausebio.damore@ethereal.email",
		to: to,
		subject: subject,
		html: html_msg,
	});

	console.log("Message Sent: %s", info.messageId)
	return info.messageId ? true : false;
}

async function sendCode(to, code){
	return await sendMail(to, "MinAuth Code", code);
}

async function sendAPIKey(to, api_key){
	return await sendMail(to, "MinAuth API Key", api_key);
}


module.exports = {
	createTestAccount,
	sendMail,
	sendCode,
	sendAPIKey
}