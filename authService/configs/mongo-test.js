const mongoose = require('mongoose');

const mongoDefaults = require('./mongo-defaults');

mongoose.connect('mongodb://mongo-test/minAuthTest', mongoDefaults);

module.exports = mongoose;
