let mongoose = require('mongoose');

const mongoDefaults = require('./mongo-defaults');

mongoose.connect('mongodb://mongo/minAuth', mongoDefaults);

let db = mongoose.connection;

db.on('connected', () => console.log(`MongoDB running on ${db.host}:${db.port}`));

module.exports = mongoose;