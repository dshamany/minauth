const SHA512 = require('js-sha512');

const User = require('./models/user');
const { uuidv4 } = require('./controllers/utilities');

const testApiKey = uuidv4();

const testUserCredentials = {
	username: 'test123',
	email: 'test@test.example',
	api_key: SHA512(testApiKey),
};

// Creates an initial user whose credentials can be used in tests.
// Use the name of this function as a before or beforeEach hook.
function createTestUser() {
	const user = new User(testUserCredentials);
	return user.save();
}

module.exports = { testApiKey, testUserCredentials, createTestUser };
